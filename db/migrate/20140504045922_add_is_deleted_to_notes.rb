class AddIsDeletedToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :is_deleted, :boolean, :default => false, :null => false
  end
end
