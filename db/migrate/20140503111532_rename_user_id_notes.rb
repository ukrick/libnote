class RenameUserIdNotes < ActiveRecord::Migration
  def change
    rename_column :notes, :user_id, :visitor_id
  end
end
