class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.references :user
      t.references :author
      t.references :publication

      t.timestamps
    end
    add_index :notes, :user_id
    add_index :notes, :author_id
    add_index :notes, :publication_id
  end
end
