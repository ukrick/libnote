class Admin::DashboardController < Admin::AdminController

  def index
    visitors = Visitor.all
    @visitors = visitors.map(&:name).reject!(&:blank?)
    authors = Author.all
    @authors = authors.map(&:name).reject!(&:blank?)
    publications = Publication.all
    @titles = publications.map(&:title).reject!(&:blank?)
    @descriptions = publications.map(&:description).reject!(&:blank?)
    @notes = Note.where('not is_deleted')
    @note = Note.new

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @notes }
    end
  end
end
