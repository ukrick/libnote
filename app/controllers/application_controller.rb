class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :store_location

  # Setting the Locale from the Client Supplied Information.
  public
  def set_locale
    #logger.debug "* Accept-Language: #{request.env['HTTP_ACCEPT_LANGUAGE']}"
    #logger.debug "* Locale set to '#{I18n.locale}'"
    I18n.locale = accept_language_header
  end

  private
  def accept_language_header
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  end

  def store_location
    # disallow return to login, logout, sign up pages
    disallowed_urls = %w(sign_up sign_in sign_out)
    disallowed_urls.map! { |url| url[/\/\w+$/] }
    unless disallowed_urls.include?(request.fullpath)
      session[:return_to] = request.fullpath.gsub('//', '/')
    end
  end
end
