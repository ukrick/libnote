class Note < ActiveRecord::Base
  belongs_to :visitor
  belongs_to :author
  belongs_to :publication

  accepts_nested_attributes_for :visitor, :author, :publication, update_only: true

  validates :publication, :presence => true

  def set_as_deleted
    update_attribute :is_deleted, true
  end
end
