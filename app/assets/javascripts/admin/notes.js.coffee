# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  availableVisitors = $("#visitors").data('visitors')
  $("#note_visitor_attributes_name").autocomplete({
    source: availableVisitors
  })
  availableAuthors = $("#authors").data('authors')
  $("#note_author_attributes_name").autocomplete({
    source: availableAuthors
  })
  availableTitles = $("#titles").data('titles')
  $("#note_publication_attributes_title").autocomplete({
    source: availableTitles
  })
  availableDescriptions = $("#descriptions").data('descriptions')
  $("#note_publication_attributes_description").autocomplete({
    source: availableDescriptions
  })
  messageVisitor = $("#note_visitor_attributes_name").data('message')
  $("#new_note").validate({
    rules: { 'note[visitor_attributes][name]': {minlength: 3} },
    messages: {'note[visitor_attributes][name]': messageVisitor}
  })