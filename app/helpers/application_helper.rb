module ApplicationHelper

  def icon_link_tag(source='/assets/icon.png', options={})
    tag('link', {
        :rel => 'shortcut icon',
        :type => 'image/x-icon',
        :href => path_to_image(source)
    }.merge(options.symbolize_keys))
  end

  def apple_icon_link_tag(source='/assets/icon.png', options={})
    tag('link', {
        :rel => 'apple-touch-icon',
        :type => 'image/x-icon',
        :href => path_to_image(source)
    }.merge(options.symbolize_keys))
  end

  def title(page_title)
    content_for(:title) { page_title }
  end

  def yield_or_default(section, default = "")
    content_for?(section) ? content_for(section) : default
  end
end
