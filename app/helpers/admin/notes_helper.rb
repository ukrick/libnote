module Admin::NotesHelper

  def link_to_note_show(record)
    link_to image_tag('notebook.png'), admin_note_path(record)
  end

  def link_to_note_edit(record)
    link_to image_tag('notebook_edit.png'), edit_admin_note_path(record)
  end

  def link_to_note_destroy(record)
    link_to image_tag('notebook_delete.png'),
            admin_note_path(record), :method => :delete, :data => {confirm: t(:note_confirm_destroy_text)}
  end
end
