module Admin::UsersHelper

  def link_to_admin_show(record)
    link_to image_tag('business_user_info.png'),
            :controller => 'users', :action => 'show', :id => record.id
  end

  def link_to_admin_edit(record)
    link_to image_tag('business_user_edit.png'),
            :controller => 'users', :action => 'edit', :id => record.id
  end

  def link_to_admin_destroy(record)
    link_to image_tag('business_user_delete.png'),
            :controller => 'users', :action => 'confirm_destroy', :id => record.id
  end
end
