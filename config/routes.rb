Rails.application.routes.draw do

  get 'admin/users/:id/confirm_destroy', :to => 'admin/users#confirm_destroy', :as => :admin_confirm_destroy

  devise_for :users, :skip => :registrations

  authenticated :user do
    root 'admin/dashboard#index', :as => :dashboard
  end

  namespace :admin do
    resources :users
    resources :notes
  end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root 'welcome#index'

end
